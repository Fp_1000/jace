#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>


int
main (void)
{


  char yes[4] = { 'y', 'e', 's', '\0' };
  char no[3] = { 'n', 'o', '\0' };
  int a, b, c;
  char sto[5];
  c = a + b;


  char exit[5] = { 'e', 'x', 'i', 't', '\0' };
  char help[5] = { 'h', 'e', 'l', 'p', '\0' };
  char abt[4] = { 'a', 'b', 't', '\0' };
  char mkfile[7] = { 'm', 'k', 'f', 'i', 'l', 'e', '\0' };
  char sum[4] = { 's', 'u', 'm', '\0' };
  char rest[5] = {'r', 'e', 's', 't','\0'};
  char init[5] = { 'i', 'n', 'i', 't', '\0' };
  char stime[6] = { 's', 't', 'i', 'm', 'e', '\0' };

  time_t t = time (NULL);
  struct tm tm = *localtime (&t);

  printf ("\n");
  printf ("Root $ ");
  scanf ("%s", &sto);
  if ((strcmp (init, sto)) == 0)
    {
      printf ("Do you want to initialize?\n");
      printf ("[Y/n]? ");
      scanf ("%s", &sto);
      if ((strcmp (yes, sto)) == 0)
	{


	  while (b > 0)
	    {
		  
	      printf("\n");
	      printf ("> ");
	      scanf ("%s", &sto);
	      if ((strcmp (sum, sto)) == 0)
		{

		  printf ("Enter two numbers to add\n");
		  scanf ("%d%d", &a, &b);
		  c = a + b;
		  printf ("Sum of the numbers = %d\n", c);

		}
	      else if ((strcmp (abt, sto)) == 0)
		{

		  printf ("            ---======HELP======---        \n");
		  printf ("            Version: Jace 0.2.0           \n");
		  printf (" Licence: GNU General Public License v3.0 \n");
		  printf ("            Made by: Josue Zafra          \n");
		}
	      else if ((strcmp (help, sto)) == 0)
		{

		  printf ("help - display the list of commands\n");
		  printf ("abt - about the program, author, etc.\n");
		  printf ("init - Initialize the shell (in progress)\n");
		  printf ("sum - Make a simple sum\n");
		  printf ("exit(not worlking yet...) - Makes you leave from the operation command section and returns to the main shell\n");
		  printf ("mkdir (not working) - Makes a direction\n");
		  printf ("stime - Shows actual date and time\n");
		  printf ("\n");
		}
	      else if ((strcmp (stime, sto)) == 0)
		{

		  printf ("now: %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900,tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,tm.tm_sec);
		  printf ("\n");
		}
		else if((strcmp (mkfile, sto)) == 0)
		{
			printf("Not ready...");
			printf("\n");
		}
		  else if((strcmp (rest, sto)) == 0)
		  {
			  printf("Enter two numbers to rest\n");
			  scanf("%d%d", &a, &b);
			  c = a - b;
			  printf("The result is : %d\n", c);
		  }
	      else
		{
		  printf ("Sorry, the command is not valid, please insert a valid command or check spelling\n");


		}

	    }


	}			//Outside of "while"
      else if ((strcmp (no, sto)) == 0)
	{

	  printf ("See you later!\n");
	}

      else
	{

	  printf ("Err...\n");
	}

    }	// Inside of ye or no 


  return 0;
}
